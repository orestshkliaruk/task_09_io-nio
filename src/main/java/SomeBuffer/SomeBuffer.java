package SomeBuffer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class SomeBuffer {
    private static Logger loggerInSomeBuffer = LogManager.getLogger(SomeBuffer.class);


    public StringBuffer readFrom( int sizeOfBuffer) {
        loggerInSomeBuffer.info("Reading...");
        File inputFile = new File("C:\\Users\\orest\\Desktop\\OrestTestingStuff.txt");
        try {
            FileInputStream fis = new FileInputStream(inputFile);
            FileChannel fileChannel = fis.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(sizeOfBuffer);
            StringBuffer stringBuffer = new StringBuffer();
            while (fileChannel.read(buffer) > 0) {
                buffer.flip();
                while (buffer.hasRemaining()) {
                    byte b = buffer.get();
                    char ch = (char) b;
                    stringBuffer.append(ch);
                }
                buffer.clear();
            }
            loggerInSomeBuffer.info(stringBuffer.toString());
            loggerInSomeBuffer.info("Reading done");

            //  System.out.println(stringBuffer.toString());
            return stringBuffer;
        } catch (IOException e) {
            loggerInSomeBuffer.error("Looks like we have FileNotFoundException here. Check your file, please");
        }
        return null;
    }

    public void writeToFile(String text) {
        loggerInSomeBuffer.info("Writing...");
        File outputFile = new File("C:\\Users\\orest\\Desktop\\OrestTestingStuffWrite.txt");
        try {
            FileOutputStream fos = new FileOutputStream(outputFile);
            FileChannel fileChannel = fos.getChannel();
            byte[] bytes = text.getBytes();
            ByteBuffer buffer = ByteBuffer.wrap(bytes);
            fileChannel.write(buffer);
            fileChannel.close();
            loggerInSomeBuffer.info("Writing done.");
        } catch (Exception e) {
            loggerInSomeBuffer.error("Looks like we have some Exception here. Check your file, please");

        }
    }
}
