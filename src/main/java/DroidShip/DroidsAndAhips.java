package DroidShip;

import org.apache.logging.log4j.*;

import java.io.Serializable;

public class DroidsAndAhips implements Serializable {

    private static Logger loggerInMain = LogManager.getLogger(DroidsAndAhips.class);

    public static void main(String[] args) {

        Ship ship = new Ship(Droid.makeDroidArmy(), 200.6, "MyLittleDroidShip");
        loggerInMain.info(ship.toString());


        Serializer ser = new Serializer();

        ser.serializeShip(ship);
        Ship shipAfterDeser = ser.deserializeShip("Ship.out");
        loggerInMain.info("Ship after deserealization: \n"+shipAfterDeser.toString());


    }
}
