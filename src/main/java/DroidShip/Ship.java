package DroidShip;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.Serializable;
import java.util.Arrays;

public class Ship implements Serializable {
    private static Logger loggerInShip = LogManager.getLogger(Ship.class);

    public String nameOfShip;
    private static final int DROID_CAPACITY = 15;
    Droid[] droidArmy;
    public double spaceSpeed;
    private double enginePower;

    public Ship(Droid[] droidArmy, double enginePower, String nameOfShip) {
        this.nameOfShip = nameOfShip;
        this.droidArmy = droidArmy;
        this.enginePower = enginePower;
        this.spaceSpeed = ((enginePower * 150) - DROID_CAPACITY);
        loggerInShip.info("The Ship was created");
    }

    public static int getDROID_CAPACITY() {
        return DROID_CAPACITY;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "nameOfShip='" + nameOfShip + '\'' +
                ", droidArmy=" + Arrays.toString(droidArmy) +
                ", spaceSpeed=" + spaceSpeed +
                ", enginePower=" + enginePower +
                '}';
    }
}
