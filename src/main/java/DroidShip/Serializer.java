package DroidShip;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class Serializer {
    private static Logger loggerInSerializer = LogManager.getLogger(Serializer.class);


    public void serializeShip(Ship ship) {
        try {

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                    new FileOutputStream("Ship.out"));
            objectOutputStream.writeObject(ship);
            objectOutputStream.close();
            loggerInSerializer.info("The Ship " + ship.nameOfShip + " was SERIALIZED successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Ship deserializeShip(String pathToFile) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream((
                    new FileInputStream((pathToFile))
            ));
            Ship shipFromFile = (Ship) objectInputStream.readObject();
            objectInputStream.close();
            loggerInSerializer.info("The Ship " + shipFromFile.nameOfShip + " was DESERIALIZED successfully");
            return shipFromFile;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }
        return null;
    }
}
