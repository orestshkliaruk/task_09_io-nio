package DroidShip;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.Serializable;
import java.util.Date;

public class Droid implements Serializable {
    private static Logger loggerInDroid = LogManager.getLogger(Droid.class);

    Date date = new Date();
    public String name;
    private final int AMMO = 250;
    private final double hp = 100.0;
    private transient String dateOfCreation = date.toString();

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", AMMO=" + AMMO +
                ", hp=" + hp +
                ", Date Of Creation=" + dateOfCreation+
                '}';
    }

    public Droid (String name){
        this.name = name;
    }

    public static   Droid[] makeDroidArmy(){
        Droid[] army = new Droid[Ship.getDROID_CAPACITY()];

        for(int i = 0; i < Ship.getDROID_CAPACITY();i++){
            army[i] = new Droid("Droid #"+i);
        }

        loggerInDroid.info("The army od Droids was created");
        return army;
    }

}
