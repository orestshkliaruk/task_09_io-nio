package FileKeksplorer;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Explorer ex = new Explorer();

        while (true){
                String directory = scanner.nextLine();
                ex.getPackageContents(directory);
        }

    }
}
