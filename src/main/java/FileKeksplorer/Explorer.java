package FileKeksplorer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class Explorer {
    private static Logger loggerInExplorer = LogManager.getLogger(Explorer.class);

    void getPackageContents(String path) {

        File folder = new File(path);
        File[] folderContent = folder.listFiles();
        ArrayList<String> content = new ArrayList<>();

        for (File file : folderContent) {
            if (file.isFile()) {
                content.add("\nFile: " + file.getName());
            } else if (file.isDirectory()) {
                content.add("\nFolder: " + file.getName());
            }
        }

        Collections.sort(content);
        loggerInExplorer.info(content.toString());
    }
}
