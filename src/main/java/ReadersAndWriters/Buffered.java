package ReadersAndWriters;

import java.io.*;

public class Buffered {
    public void read(String pathToFile) {
        BufferedReader bufferedReader = null;
        try {
            File file = new File(pathToFile);

            // bufferedReader = new BufferedReader(new FileReader(file), 10);
            // bufferedReader = new BufferedReader(new FileReader(file), 50);
            // bufferedReader = new BufferedReader(new FileReader(file), 100);
            //  bufferedReader = new BufferedReader(new FileReader(file), 5000);
            // bufferedReader = new BufferedReader(new FileReader(file), 500000);
            bufferedReader = new BufferedReader(new FileReader(file), 50000000);
            //Obviously, the bigger buffer we have, the faster goes the reading process

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void write(String pathWhereToWrite){
            try{
                File file = new File(pathWhereToWrite);
                FileWriter fileWriter = new FileWriter(file);
                // BufferedWriter bufferedWriter = new BufferedWriter(fileWriter, 10);
                //BufferedWriter bufferedWriter = new BufferedWriter(fileWriter, 50);
                //BufferedWriter bufferedWriter = new BufferedWriter(fileWriter, 500);
                //BufferedWriter bufferedWriter = new BufferedWriter(fileWriter, 5000);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter, 500000);
                // BufferedWriter bufferedWriter = new BufferedWriter(fileWriter, 500000000);

                for (int i = 0; i < 23809523; i++) {
                    bufferedWriter.write("-OLOLOLO=");
                    if (i % 100 == 0){
                        bufferedWriter.write("\n");
                    }
                }
                bufferedWriter.close();
            }catch (IOException e){
                e.printStackTrace();
            }


    }
}
