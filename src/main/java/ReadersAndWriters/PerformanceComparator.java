package ReadersAndWriters;

import org.apache.logging.log4j.*;

public class PerformanceComparator {
    private static Logger loggerInPerformanceComparator = LogManager.getLogger(PerformanceComparator.class);

    public static void main(String[] args) {

        Buffered buffered = new Buffered();
      // buffered.read("C:\\Users\\orest.shkliaruk\\Desktop\\OrestTestingShit.txt");
       // buffered.write("C:\\Users\\orest.shkliaruk\\Desktop\\OrestTestingShit.txt");

        NonBuffer nonbuffered = new NonBuffer();
        nonbuffered.write("C:\\Users\\orest\\Desktop\\OrestTestingStuff.txt");
        nonbuffered.read("C:\\Users\\orest\\Desktop\\OrestTestingStuff.txt");
    }
}
