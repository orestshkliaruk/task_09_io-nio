package ReadersAndWriters;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class NonBuffer {
    public void read(String pathToFile) {
        FileReader reader = null;
        try {
            File file = new File(pathToFile);

            reader = new FileReader(file);

            char[] chars = new char[(int) file.length()];

            reader.read(chars);

            for (char c : chars) {
                System.out.print(c);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void write(String pathWhereToWrite) {
        try {
            File file = new File(pathWhereToWrite);
            FileWriter fileWriter = new FileWriter(file);

            for (int i = 0; i < 23809523; i++) {
                fileWriter.write("-OLOLOLO=");
                if (i % 100 == 0) {
                    fileWriter.write("\n");
                }
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}
