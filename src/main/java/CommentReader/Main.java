package CommentReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger loggerInMain = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        CommentReader reader = new CommentReader();
        String comments;
        comments = reader.readComments("C:\\Users\\orest\\IdeaProjects\\task_09_io-nio\\src\\main\\java\\MyInputStream\\MyInputStream.java");
        loggerInMain.info(comments);
    }
}
