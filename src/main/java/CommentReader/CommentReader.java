package CommentReader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

class CommentReader {
    private CharSequence simple = "//";
    private CharSequence start = "/*";
    private CharSequence stop = "*/";

    String readComments(String pathToFile) {
        String res = null;
        try (
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(
                                new FileInputStream(pathToFile), StandardCharsets.UTF_8))) {
            String line;
            StringBuilder result = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                if (line.contains(simple)) {
                    result.append("\n" + line);
                }
                if (line.contains(start)) {
                    while ((((line = reader.readLine())) != null) && !(line.contains(stop))) {
                        result.append("\n" + line);
                    }
                }

            }
            res = result.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

}
